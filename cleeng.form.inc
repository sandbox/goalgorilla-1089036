<?php
// $Id: cleeng.form.inc,v 1.0 2010/10/24 17:20:20 sun Exp $

/**
 * @file
 * The form of user edit setting
 */
?>

<div id="cleeng-contentForm" title="Cleeng: Create new content element" style="display:none;">
<form action="" method="post" style="position: relative;">
    <fieldset>   
      <div class="form-item">
        <label class="cleeng-ContentForm-wide" for="CleengWidget-ContentForm-Description">
    <?php echo t('Description') ?>
            (<span id="cleeng-ContentForm-DescriptionCharsLeft">110</span> <?php echo t('characters left') ?>)
        </label>
        <textarea class="cleeng-ContentForm-wide" rows="2" cols="50" name="CleengWidget-ContentForm-Description" id="cleeng-ContentForm-Description" class="text ui-widget-content ui-corner-all">
        </textarea>
	  </div>
	  <div class="form-item">
        <label class="cleeng-ContentForm-wide" for="CleengWidget-ContentForm-Price"><?php echo t('Price') ?>: $<span id="cleeng-ContentForm-PriceValue">0.00</span></label>
        <input type="hidden" name="CleengWidget-ContentForm-Price" id="cleeng-ContentForm-Price" value="" class="text ui-widget-content ui-corner-all" />
	  </div>
        <div id="cleeng-ContentForm-PriceSlider"></div>
	  <div class="form-item">
        <label for="CleengWidget-ContentForm-LayerDatesEnabled">
			<input type="checkbox" id="cleeng-ContentForm-LayerDatesEnabled" >
        <?php echo t('Enable layer dates.') ?></label>
	  </div>
        <div id="cleeng-ContentForm-LayerDates">
		  <table border="0">
		    <tr>
			  <td><?php echo t('from') ?>:</td>
			  <td><input type="text" id="cleeng-ContentForm-LayerStartDate"
                       name="layerStartDate" value="<?php echo date('Y-m-d') ?>" >&nbsp;
                  <select type="text" id="cleeng-ContentForm-LayerStartTimeHour" disabled="disabled"
                       name="layerStartTimeHour" class="cleeng-ContentForm-timeselector" >
                         <?php for ($i=0;$i<=24;$i++) {
                            $value = ($i<10) ? '0' . $i : $i;
                            print '<option value="' . $value . '">' . $value . '</option>';
                         }?>
                       </select>:
                  <select type="text" id="cleeng-ContentForm-LayerStartTimeMinute" disabled="disabled"
                       name="layerStartTimeMinute" class="cleeng-ContentForm-timeselector" >
                         <?php for ($i=0;$i<=60;$i++) {
                            $value = ($i<10) ? '0' . $i : $i;
                            print '<option value="' . $value . '">' . $value . '</option>';
                         }?>
                       </select></td>
			</tr>
		    <tr>
			  <td><?php echo t('to') ?>:</td>
			  <td><input type="text" id="cleeng-ContentForm-LayerEndDate"
                       name="layerEndDate" value="<?php echo date('Y-m-d', time()+3600*24*7) ?>">&nbsp;
                  <select type="text" id="cleeng-ContentForm-LayerEndTimeHour" disabled="disabled"
                       name="layerEndTimeHour" class="cleeng-ContentForm-timeselector" >
                         <?php for ($i=0;$i<=24;$i++) {
                            $value = ($i<10) ? '0' . $i : $i;
                            print '<option value="' . $value . '">' . $value . '</option>';
                         }?>
                       </select>:
                  <select type="text" id="cleeng-ContentForm-LayerEndTimeMinute" disabled="disabled"
                       name="layerEndTimeMinute" class="cleeng-ContentForm-timeselector" >
                         <?php for ($i=0;$i<=60;$i++) {
                            $value = ($i<10) ? '0' . $i : $i;
                            print '<option value="' . $value . '">' . $value . '</option>';
                         }?>
                       </select></td>
			</tr>
		  </table>
             
        </div>
	  <div class="form-item">
        <label for="CleengWidget-ContentForm-ReferralProgramEnabled">
		<input type="checkbox" id="cleeng-ContentForm-ReferralProgramEnabled" >
        <?php echo t('Enable referral program') ?></label>
	  </div>
	  <div class="form-item">
        <label class="cleeng-ContentForm-wide" for="CleengWidget-ContentForm-ReferralRate"><?php echo t('Referral rate') ?>: <span id="cleeng-ContentForm-ReferralRateValue">5</span>%</label>
        <input type="hidden" name="CleengWidget-ContentForm-ReferralRate" id="cleeng-ContentForm-ReferralRate" value="" class="text ui-widget-content ui-corner-all" />
	  </div>
        <div id="cleeng-ContentForm-ReferralRateSlider"></div>
    </fieldset>
</form>
<div id="cleeng-contentForm-info"></div>
</div>