<?php

/**
 * Simple class for handling Cleeng Client's exceptions
 */
class CleengClientException extends Exception
{
    
}

/**
 * CleengClient - helper class for accessing Cleeng API
 *
 * @version 0.9.1
 */
class CleengClient
{
    /**
     * Platform URLs
     */
    const SANDBOX = 'sandbox.cleeng.com';
    const PRODUCTION = 'cleeng.com';
    const STAGING = 'staging.cleeng.com';

    /**
     * Base URL to Cleeng server
     * @var string
     */
    protected $_platformUrl = 'sandbox.cleeng.com';

    /**
     * Name of the cookie that is used for storing OAuth token
     * @var string
     */
    protected $_cookieName = 'CleengClientAccessToken';

    /**
     * Client identifier
     * @var string
     */
    protected $_clientId;

    /**
     * Client secret password
     * @var string
     */
    protected $_clientSecret;

    /**
     * URL that user will be redirected to after loging in/purchasing content
     * @var string
     */
    protected $_callbackUrl;

    /**
     * If set to true, Cleeng Platform website will appear with compact layout
     *
     * @var boolean
     */
    protected $_popupWindowMode = true;

    /**
     *
     * @var string
     */
    protected $_accessToken;

    /**
     * Default cookie save path
     * @var string
     */
    protected $_cookiePath = '/';

    /**
     * Information about currently authenticated user
     * @var array
     */
    protected $_userInfo;

    /**
     * Raw output from Cleeng API - can be used for debugging purposes
     * @var string
     */
    protected $_apiOutputBuffer;

    /**
     * Class constructor. Can be used to pass options such as
     * callback URL, client ID or client secret
     * @param array $config
     */
    public function __construct($options = null)
    {
        if (null !== $options) {
            $this->setOptions($options);
        }
    }    

    /**
     * Set multiple options
     * @param array $options
     */
    public function setOptions($options)
    {
        if (!is_array($options)) {
            throw new CleengClientException('Config must be an array.');
        }
        foreach ($options as $name => $value) {
            $this->setOption($name, $value);
        }
    }

    /**
     * Set an option
     * @param string $name
     * @param mixed $value
     * @return CleengClient $this provides fluent interface
     */
    public function setOption($name, $value)
    {
        $propName = '_' . $name;
        if (!property_exists($this, $propName)) {
            throw new CleengClientException("Unrecognized option: '$name'");
        }
        $methodName = 'set' . ucfirst($name);
        if (method_exists($this, $methodName)) {
            $this->$methodName($name, $value);
        } else {
            $this->$propName = $value;
        }
        return $this;
    }

    /**
     * Returns given option value
     * @param string $name
     * @return mixed
     */
    public function getOption($name)
    {
        $propName = '_' . $name;
        if (!property_exists($this, $propName)) {
            throw new CleengClientException("Unrecognized option: '$name'");
        }
        $methodName = 'get' . ucfirst($name);
        if (method_exists($this, $methodName)) {
            return $this->$methodName($name);
        } else {
            return $this->$propName;
        }
    }

    /**
     * Set access token
     * @param string $token
     */
    public function setAccessToken($token)
    {
        $this->_accessToken = $token;
    }

    /**
     * Returns access token.
     * @return string
     */
    public function getAccessToken()
    {
        if (null === $this->_accessToken) {            
            $this->_accessToken = $this->loadAccessToken();
        }
        return $this->_accessToken;
    }

    /**
     * Returns true if user is authorized in Cleeng Website
     * @return bool
     */
    public function isUserAuthenticated()
    {
        return ((bool)$this->getAccessToken() && (bool)$this->getUserInfo());
    }

    /**
     * Redirects user to Cleeng website where he is asked to authorize widget
     */
    public function authenticate()
    {
        $clientId = $this->getOption('clientId');
        $clientSecret = $this->getOption('clientSecret');
        $callbackUrl = $this->getOption('callbackUrl');

        if (!$clientId || !$clientSecret || !$callbackUrl) {
            throw new CleengClientException('Following options are required for authentication process: clientId, clientSecret, callbackUrl.');
        }

        $url = $this->getUrl('oauth') . '/authenticate?';
        $params = array(
            'client_id' => $clientId,
            'response_type' => 'code',            
            'redirect_uri' => $callbackUrl
        );
        if ($this->getOption('popupWindowMode')) {
            $params['popup'] = 1;
        }
        header('Location:' . $url . http_build_query($params, '', '&'));
        exit;
    }

    /**
     * Logout (simply destroys access token)
     */
    public function logout()
    {
        $this->callApi('logout');
        $this->destroyAccessToken();
        $this->setAccessToken(null);
    }

    /**
     * Redirect user to Cleeng purchaseContent page
     * @param int $contentId
     */
    public function purchaseContent($contentId)
    {
        // filter $contentId
        $contentId = (int)$contentId;

        $url = $this->getUrl('oauth') . '/authenticate?';
        $params = array(
            'client_id' => $this->getOption('clientId'),
            'response_type' => 'code',
            'purchase_content_id' => $contentId,
            'redirect_uri' => $this->getOption('callbackUrl')
        );
        if ($this->isUserAuthenticated()) {
            // User will get new token after purchasing content,
            // but we need to pass old one to check if the same
            // user is authenticated on widet and on Cleeng Website
            $params['oauth_token'] = $this->getAccessToken();    
        }
        if ($this->getOption('popupWindowMode')) {
            $params['popup'] = 1;
        }
        header('Location:' . $url . http_build_query($params, '', '&'));
        exit;
    }

    /**
     * Handle OAuth callback (usually simply retrieve access token)
     */
    public function processCallback()
    {
        $clientId = $this->getOption('clientId');
        $clientSecret = $this->getOption('clientSecret');
        $callbackUrl = $this->getOption('callbackUrl');

        if (!$clientId || !$clientSecret || !$callbackUrl) {
            throw new CleengClientException('Following options are required for authentication process: clientId, clientSecret, callbackUrl.');
        }

        if (isset($_REQUEST['code'])) {
            $url = $this->getUrl('oauth') . '/token';
            $params = array(
                'client_id' => $this->getOption('clientId'),
                'client_secret' => $this->getOption('clientSecret'),
                'grant_type' => 'authorization-code',
                'code' => $_REQUEST['code'],
//                'redirect_uri' => $this->getOption('callbackUrl')
            );

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, '', '&'));

            /**
             * TODO: Remove password, validate certificate
             */
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, "cleengtest:Manta2010");

            $buffer = curl_exec($ch);
            $this->_apiOutputBuffer = $buffer;

            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($code !== 200 && $code !== 302) {
                throw new CleengClientException("Authorization error ($code).");
            }

            $params = json_decode($buffer, true);

            if (isset($params['access_token'])) {
                $this->storeAccessToken($params['access_token'],
                                        time()+$params['expires_in']);
                $this->_accessToken = $params['access_token'];
            }
        }
    }

    /**
     * Load access token from cookie
     * This method can be overwritten in child class if (for example) someone would
     * like to store access token in DB
     *
     * @return string|null
     */
    public function loadAccessToken()
    {
        // read access token from cookie?
        if (isset($_COOKIE[$this->_cookieName])) {
            return $_COOKIE[$this->_cookieName];
        }
        return null;

    }

    /**
     * Save acess token in cookie.
     * This method can be overwritten in child class if (for example) someone would
     * like to store access token in DB
     *
     * @param string $accessToken
     * @param int $expires
     */
    public function storeAccessToken($accessToken, $expires)
    {
        setcookie($this->_cookieName, $accessToken, $expires, $this->_cookiePath);
    }

    /**
     * Removes cookie with access token
     */
    public function destroyAccessToken()
    {
        setcookie($this->_cookieName,
                  $this->getAccessToken(),
                  time()-1000,
                  $this->_cookiePath);
    }

    /**
     * Return platform URL
     * 
     * @param string $type
     * @return string URL
     */
    public function getUrl($type=null)
    {
        switch ($type) {
            case 'oauth':
                return 'https://' . $this->_platformUrl . '/oauth';
            case 'logo':
                return 'http://' . $this->_platformUrl . '/logo';
            case 'api':
                return 'https://' . $this->_platformUrl . '/api/json';
            case 'autologin':
                return 'https://' . $this->_platformUrl . '/autologin/autologin.js';
            default:
                return 'http://' . $this->_platformUrl;
        }
    }

    /**
     * Returns logo url
     * @param string $size
     * @param integer $contentId
     */
    public function getLogoUrl($contentId, $type='cleeng-tagline', $width='500')
    {        
        $params = 'contentId=' . (int)$contentId;
        if ($this->isUserAuthenticated()) {
            $params .= '&amp;oauth_token=' . urlencode($this->getAccessToken());
        }
        return $this->getUrl('logo') . '/' . $type . '-' . $width . '.png?' . $params;
    }

    /**
     * Return URL to autologin javascript file
     * @return string
     */
    public function getAutologinScriptUrl()
    {
        return $this->getUrl('autologin');
    }

    /**
     * Helper function for calling Cleeng API
     *
     * @param string $method method to call
     * @param array $params method parameters (see API documentation)
     * @return array
     */
    public function callApi($method, $params = array())
    {
        $urlParams = array();
        if ($token = $this->getAccessToken()) {
            $urlParams['oauth_token'] = $token;
        }
        $ch = curl_init($this->getUrl('api') . '?' . http_build_query($urlParams, '', '&'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);

        $jsonParams = array(
            'method' => $method,
            'params' => $params,
            'id' => 1
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonParams));
        $buffer = curl_exec($ch);
        $this->_apiOutputBuffer = $buffer;
        
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code !== 200 && $code !== 302) {
            throw new CleengClientException("Authorization error ($code).");
        }

        if ($buffer) {
            $bufferArray = @json_decode($buffer, true);

            if (!is_array($bufferArray)) {
                throw new CleengClientException('Server error: ' . $buffer);
            }

            return $bufferArray['result'];
        } else {
            throw new CleengClientException('Unable to call remote procedure.');
        }
    }

    /**
     * Returns raw output from last API call.
     * Function can be used for debugging purposes.
     * @return string
     */
    public function getApiOutputBuffer()
    {
        return $this->_apiOutputBuffer;
    }

    /**
     * Cleeng API calls
     *
     * Following methods reflect Cleeng APIs.
     */

    /**
     * Returns information about currently authenticated user
     */
    public function getUserInfo()
    {
        if (null === $this->_userInfo) {
            if (!$this->getAccessToken()) {
                throw new CleengClientException('Method getUserInfo() requires valid access token.');
            }

            $ret = $this->callApi('getUserInfo');

            if ($ret['success'] == false) {

                /**
                 * Don't throw exception when user is not authorized - just return null.
                 * This will prevent throwing exception when uswitching servers,
                 * for example from sandbox to production
                 *
                 * TODO: This behaviour will likely change (we need to come with
                 * some consistent solution).
                 */
                if ($ret['errorCode'] == 'ERR_NO_AUTH') {
                    return null;
                }

                throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
            }
            $this->_userInfo = $ret['userInfo'];
        }
        return $this->_userInfo;
    }

    /**
     * Check if user has purchased given content
     * @param array $ids
     * @return bool
     */
    public function isContentPurchased($ids)
    {
        if (!$this->isUserAuthenticated()) {
            throw new CleengClientException('Method isContentPurchased() requires authenticated user.');
        }

        if (!is_array($ids)) {
            throw new CleengClientException('Method isContentPurchased() requires array of content IDs as its argument.');
        }

        $ret = $this->callApi('isContentPurchased', array($ids));

        if ($ret['success'] == false) {
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return $ret;
    }

    /**
     * Updates existing digital content
     * @param array $contentData
     * @return integer
     */
    public function updateContent($contentData)
    {
        if (!$this->isUserAuthenticated()) {
            throw new CleengClientException('Method updateContent() requires authenticated user with publisher account.');
        }

        if (!is_array($contentData)) {
            throw new CleengClientException('Method updateContent() requires array of content as its argument.');
        }

        $ret = $this->callApi('updateContent', array($contentData));
        if ($ret['success'] == false) {            
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return $ret['content'];
    }

    /**
     * Creates existing digital content
     * @param array $contentData
     * @return integer
     */
    public function createContent($contentData)
    {
        if (!$this->isUserAuthenticated()) {
            throw new CleengClientException('Method createContent() requires authenticated user with publisher account.');
        }

        if (!is_array($contentData)) {
            throw new CleengClientException('Method createContent() requires array of content as its argument.');
        }

        $ret = $this->callApi('createContent', array($contentData));
        if ($ret['success'] == false) {
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return $ret['content'];
    }

    /**
     * Removes contents
     * @param array $ids
     * @return integer
     */
    public function removeContent($ids)
    {
        if (!$this->isUserAuthenticated()) {
            throw new CleengClientException('Method getContentList() requires authenticated user with publisher account.');
        }

        if (!is_array($ids)) {
            throw new CleengClientException('Method removeContent() requires array of content IDs as its argument.');
        }

        $ret = $this->callApi('removeContent', array($ids));

        if ($ret['success'] == false) {
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return true;
    }
   
    /**
     * Returns information (like rating) about multiple contents
     * @param array $contentIds
     */
    public function getContentInfo($ids)
    {
        $ret = $this->callApi('getContentInfo', array($ids));
        if (!is_array($ids)) {
            throw new CleengClientException('Method getContentInfo() requires array of content IDs as its argument.');
        }
        if ($ret['success'] == false) {
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return $ret['contentInfo'];
    }

    /**
     * Returns information (like rating) about multiple contents
     *
     * @todo: This method will likely be removed as it seems that nobody needs it :)
     *
     * @param array $contentIds
     */
    public function getContentDescription($ids)
    {
        if (!is_array($ids)) {
            throw new CleengClientException('Method getContentDescription() requires array of content IDs as its argument.');
        }
        $ret = $this->callApi('getContentDescription', array($ids));
        if ($ret['success'] == false) {
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return $ret['contentDescription'];
    }

    /**
     * Tell platform whether user liked content or not
     *
     * @param integer $contentId
     * @param boolean $liked
     * @return boolean
     */
    public function vote($contentId, $liked)
    {
        $ret = $this->callApi('vote', array($contentId, $liked));        
        if ($ret['success'] == false) {
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return $ret['voted'];
    }

    /**
     * Tell platform that user clicked on "cleeng it" link, so that it can
     * increase content rating.
     *
     * @param integer $contentId
     * @return boolean
     */
    public function referContent($contentId)
    {
        $ret = $this->callApi('referContent', array($contentId));
        if ($ret['success'] == false) {
            throw new CleengClientException('WebAPI Error: ' . $ret['errorDescription']);
        }
        return $ret['referred'];
    }

    /**
     * Automatically authenticate user if he is logged on the platform.
     * This required $sessionId and $key params, which are obtained
     * from cleeng.com/autologin/autologin.js Javascript file.
     *
     * @param string $sessionId
     * @param string $key
     * @return bool true if succeeded
     */
    public function autologin($sessionId, $key)
    {
        $ret = $this->callApi('autologin', array($this->getOption('clientId'), $sessionId, $key));
        
        if ($ret['valid']) {
            $_REQUEST['code'] = $ret['code'];
            $this->processCallback();

            if ($this->isUserAuthenticated()) {
                return true;
            }
        }
        return false;        
    }
}
