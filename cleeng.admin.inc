<?php
// $Id: cleeng.admin.inc,v 1.0 2010/10/24 18:21:29 sun Exp $

/**
 * @file
 * The form of admin setting
 */

/**
 * cleeng parameters form for admin console
 */
function cleeng_admin_setting() {
  $form['cleeng_clientid'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Client ID'),
    '#default_value' => variable_get('cleeng_clientid', 'b200b11ba9de'),
  );
  
  $form['cleeng_clientsecret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#required' => TRUE,
    '#default_value' => variable_get('cleeng_clientsecret', '9bb7b4e124549905585e'),
  );
  
  $form['cleeng_popupwindowmode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Popup Window'),
    '#required' => TRUE,
    '#default_value' => variable_get('cleeng_popupwindowmode', TRUE),
  );
  
  $form['cleeng_env'] = array(
    '#type' => 'select',
    '#title' => t('Cleeng Environment'),
    '#required' => TRUE,
    '#options' => array('sandbox' => t('Sandbox'), 'prod' => t('Production')),
    '#default_value' => variable_get('cleeng_env', 'prod'),
  );
  
  return system_settings_form($form);
  
}