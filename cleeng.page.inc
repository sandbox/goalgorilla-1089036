<?php
// $Id: cleeng.page.inc,v 1.0 2010/10/24 20:40:04 sun Exp $

/**
 * @file
 * The page callback for cleeng ajax
 */

/**
 * The cleeng server callback for the case of user login and purchase
 */
function cleeng_callback() {

  $cleeng = cleeng_client();
  $cleeng->processCallback();
  
  // user login
  if ($_GET['cleengMode'] == 'loginCallback') {
    print '<script>opener.CleengWidget.authUser();</script>';
  }
  // user purchase success
  elseif ($_GET['cleengMode'] == 'purchaseCallback') {
    print '<script>opener.CleengWidget.getContentInfo();/*opener.CleengWidget.markContentPurchased(); opener.CleengWidget.showPurchasedContent();*/</script>';
  }
  
  print '<script>window.close();</script>';
}

/**
 * The request for the browser ajax request
 */
function cleeng_ajax_request() {
  $mode = @$_REQUEST['cleengMode'];
  
  $cleeng = cleeng_client();
  
  switch ($mode)  {
    case 'getLogoURL':
      /**
       * Get cleeng logo
       */
      echo $cleeng->getLogoUrl($_REQUEST['contentId'], $_REQUEST['logoId'], $_REQUEST['logoWidth'], $_REQUEST['logoLocale']);
      break;
    case 'auth':
      /**
       * redirect to Cleeng authentication page for user login
       */
      $cleeng->setOption('callbackUrl', url('cleeng/callback', array('absolute' => TRUE, 'query' => array('cleengMode' => 'loginCallback', 'cleengPopup' => 1, 'backendWidget' => @$_REQUEST['backendWidget']))));
      $cleeng->authenticate();
      break;
    case 'loginCallback':
      /**
       * Cleeng will return here after authentication. We tell CleengClient
       * to proceed with OAuth stuff, and close popup window.
       * Parent window is refreshed so that widget will know that user is authenticated
       * status.
       */      
      $cleeng->setOption('callbackUrl', url('cleeng/callback', array('absolute' => TRUE, 'query' => array('cleengMode' => 'loginCallback', 'cleengPopup' => 1, 'backendWidget' => @$_REQUEST['backendWidget']))));
      $cleeng->processCallback();            
      echo '<script type="text/javascript">
          opener.Cleeng' . ($_REQUEST['backendWidget'] == TRUE ? 'BE' : 'FE') . 'WidgetWP.authUser();
          self.close();
        </script>';
      break;
    case 'purchase':
      /**
       * Purchase content
       */
      $cleeng->setOption('callbackUrl', url('cleeng/callback', array('absolute' => TRUE, 'query' => array('cleengMode' => 'purchaseCallback', 'cleengPopup' => 1, 'contentId' => @$_REQUEST['contentId']))));
      try {
        $cleeng->purchaseContent((int)$_REQUEST['contentId']);
      } catch (Exception $e) {
        print_r($cleeng->getApiOutputBuffer());
      }
      break;
    case 'purchaseCallback':
      $cleeng->setOption('callbackUrl', url('cleeng/callback', array('absolute' => TRUE, 'query' => array('cleengMode' => 'purchaseCallback', 'cleengPopup' => 1, 'contentId' => @$_REQUEST['contentId']))));
      $cleeng->processCallback();
      if ($cleeng->isUserAuthenticated())  {
        $resp = $cleeng->isContentPurchased(array((int)$_REQUEST['contentId']));
        if ($resp[(int)$_REQUEST['contentId']] == TRUE)
          echo '<script type="text/javascript">
              opener.CleengFEWidgetWP.authUser();
            opener.CleengFEWidgetWP.getcontentinfo();
              self.close();
          </script>';
        else
          echo '<script type="text/javascript">
            self.close();
          </script>';
        exit;
      }
      else {
        echo '
            <script type="text/javascript">
          opener.CleengFEWidgetWP.authUser();
          self.close();
        </script>';
        exit;
      }    
      break;
    case 'logout':
      /**
       * Disconnect from Cleeng
       */
      $cleeng->logout();
      break;
    case 'getUserInfo' :
      header('content-type: text/json');
      if ($cleeng->isUserAuthenticated()) {
        try {
          echo json_encode($cleeng->getUserInfo(@$_REQUEST['backendWidget']));
        } catch (Exception $e) {
          print_r($cleeng->getApiOutputBuffer());
        }
      }
      else {
        echo json_encode(array('auth' => FALSE));
      }
      break;
      /**
       * load content to show in browser
       */
    case 'getContentInfo' :
      try  {
        $ids = array();
        $contentinfo = array();
        if (isset($_REQUEST['content']) && is_array($_REQUEST['content']))  {
          $content = array();
          $ids = array();
          foreach ($_REQUEST['content'] as $c)  {
            $id = intval(@$c['id']);
            $postid = intval(@$c['postId']);
            $page = @$c['page'];
            if ($id && $postid && $page)  {
              $ids[] = $id;
              $content[$id] = array(
                'postId' => $postid,
                'page' => $page
              );
            }
          }

          if (count($ids))  {
            $contentinfo = $cleeng->getcontentinfo($ids);
            if (sizeof($contentinfo))  {
              //define('WP_USE_THEMES', FALSE);
              //define('CLEENG_CONTENT_SERVICE', TRUE);
              //require('../../../wp-load.php');
              foreach ($contentinfo as $key => $val)  {
                if ($val['purchased'] == TRUE)  {
                  $contentinfo[$key]['content'] = cleeng_getcontent($content[$key]['postId'], $content[$key]['page'], $key);
                }
              }
            }
          }
        }            
        header('content-type: text/json');
        echo json_encode($contentinfo);
      } catch (Exception $e)  {
        header('content-type: text/json');
        echo json_encode($cleeng->getApiOutputBuffer());
      }
      break;
      /**
       * save the content by ajax, but it's not used in drupal
       */
    case 'saveContent':
      if ($cleeng->isUserAuthenticated())  {
        $data = array(
          'itemType' => @$_POST['itemType'],
          'pageTitle' => @$_POST['pageTitle'],
          'url' => @$_POST['url'],
          'price' => @$_POST['price'],
          'shortDescription' => @$_POST['shortDescription'],
          'referralProgramEnabled' => (bool) @$_POST['referralProgramEnabled'],
          'referralRate' => @$_POST['referralRate'],
          'hasLayerDates' => (bool) @$_POST['hasLayerDates'],
          'layerStartDate' => @$_POST['layerStartDate'],
          'layerEndDate' => @$_POST['layerEndDate']
        );
        header('content-type: text/json');
        try  {

          if (isset($_POST['contentId']) && $_POST['contentId']) {
            $data['contentId'] = intval($_POST['contentId']);
            $ret = $cleeng->updateContent(array($_POST['contentId'] => $data));
          }
          else {
            $ret = $cleeng->createContent(array($data));
          }

          echo json_encode($ret);
        } catch (Exception $e)  {
          print_r($cleeng->getApiOutputBuffer());
        }
      }
      else {
        echo json_encode(array('response' => FALSE, 'errorCode' => 'ERR_NO_AUTH'));
      }
      break;
    case 'vote':
      header('content-type: text/json');
      if ($cleeng->isUserAuthenticated())  {

        $contentid = intval(@$_REQUEST['contentId']);
        $liked = intval(@$_REQUEST['liked']);

        if ($contentid > 0 && ($liked == 0 || $liked == 1))  {
          $ret = $cleeng->vote($contentid, $liked);
          echo json_encode($ret['voted']);
        } 
      }
      break;
    case 'refer':
      header('content-type: text/json');
      if ($cleeng->isUserAuthenticated())  {
        $contentid = intval(@$_REQUEST['contentId']);
        if ($contentid > 0)  {
          $ret = $cleeng->referContent($contentid);
          echo json_encode($ret['referred']);
        }
      }
      break;
      /**
       * check user login when load the page.
       */
    case 'autologin':
      $id = trim(@$_REQUEST['id']);
      $key = trim(@$_REQUEST['key']);

      header('content-type: text/json');

      $cleeng->setOption('callbackUrl', url('cleeng/callback', array('absolute' => TRUE, 'query' => array('cleengMode' => 'loginCallback', 'cleengPopup' => 1, 'backendWidget' => @$_REQUEST['backendWidget']))));
      if ($cleeng->isUserAuthenticated())  {
        echo json_encode(array('success' => TRUE));
        exit;
      }

      if ($id && $key)  {
        $ret = $cleeng->autologin($id, $key);
        echo json_encode(array('success' => $ret));
        exit;
      }
  }
}