<?php
// $Id: cleeng.render.tpl.php,v 1.0 2010/10/24 17:20:20 sun Exp $

/**
 * @file
 * The template to render cleeng content when viewing the node
 */
?>

    <div id="cleeng-layer-<?php echo $attrs['id']; ?>" class="cleeng-layer">
        <div class="cleeng-logo">
            <img src="<?php echo $cleeng->getLogoUrl($attrs['id'], 'cleeng-tagline', 630) ?>" alt="Cleeng" />
        </div>
        <div class="cleeng-text">
            <div class="cleeng-itemType"></div>
            <div class="cleeng-title"><?php echo $node->title; ?></div>
            <div class="cleeng-description"><?php echo htmlspecialchars($attrs['description']) ?></div>
            <div class="cleeng-noauth">
                <a class="cleeng-hlink cleeng-login" href="javascript:">Log-in</a>
                or
                <a class="cleeng-hlink" target="blank" href="<?php echo CLEENG_LOGIN_URL ?>">Register</a>
                to reveal this content
            </div>
            <div class="cleeng-auth">
                Welcome, <a class="cleeng-username" href="<?php echo CLEENG_ACCOUNT_URL ?>"></a>.
                <a class="cleeng-hlink cleeng-logout" href="#">Logout</a>
                <span class="cleeng-free-content-views">
                    You have <span></span> free purchase left (out of 5).
                </span>
            </div>
            <div class="cleeng-rating">
                Cleeng user rating <a class="cleeng-hlink" target="_blank" href="#" title="This is the average rating from Cleeng users who bought this content.">?</a>
                <div class="cleeng-stars cleeng-stars-0"></div>
            </div>

            <div class="cleeng-textBottom">
                <div class="cleeng-purchaseInfo">
                    <img class="cleeng-shoppingCart" src="<?php echo base_path().drupal_get_path('module', 'cleeng'); ?>/images/shopping-cart.png" alt="Shopping cart" />
                    <a class="cleeng-buy" href="#">
                        <img src="<?php echo base_path().drupal_get_path('module', 'cleeng'); ?>/images/buy-button.png" width="96" height="39" alt="Buy" />
                    </a>
                    <span class="cleeng-price" ></span>
                </div>
                <div class="cleeng-whatsCleeng">
                        What is <a href="<?php echo CLEENG_URL ?>">Cleeng?</a>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    // <![CDATA[
    jQuery('#cleeng-layer-<?php echo $attrs['id'] ?>').data('postId', '<?php echo $node->nid; ?>');
    //jQuery('#cleeng-layer-<?php echo $attrs['id'] ?>').data('page', '<?php echo url('node/'.$node->nid, array('absolute' => true)); ?>');
    jQuery('#cleeng-layer-<?php echo $attrs['id'] ?>').data('page', '1');
    // ]]>
    </script>
    <div id="cleeng-nolayer-<?php echo $attrs['id'] ?>" class="cleeng-nolayer" style="display: none">
        <div class="cleeng-nolayer-top">
            <img src="<?php echo base_path().drupal_get_path('module', 'cleeng'); ?>/images/cleeng-small.png" alt="Cleeng: Instant access to quality content" />
            <div class="cleeng-noauth">
            &nbsp;
            </div>
            <div class="cleeng-auth">
                Welcome, <a class="cleeng-username" href="<?php echo CLEENG_ACCOUNT_URL ?>"></a>.
                <a class="cleeng-hlink cleeng-logout" href="#">Logout</a>
            </div>
        </div>

        <div class="cleeng-content">

        </div>

        <div class="cleeng-nolayer-bottom">
            <div class="cleeng-rating">
                Cleeng user rating:
            </div>
            <div class="cleeng-stars cleeng-stars-0"></div>

            <a href="#" class="cleeng-vote-liked">
                <img src="<?php echo base_path().drupal_get_path('module', 'cleeng'); ?>/images/up.png" alt="Click here if you liked this content" title="Click here if you liked this content"/>
            </a>
            <a href="#" class="cleeng-vote-didnt-like">
                <img src="<?php echo base_path().drupal_get_path('module', 'cleeng'); ?>/images/down.png" alt="Click here if you didn't like this content" title="Click here if you didn't like this content"/>
            </a>
            <span class="cleeng-referral-rate">
                Referral rate:
                <span></span>
            </span>
            <a href="#" class="cleeng-it">
                Cleeng It!
            </a>
            <div class="cleeng-share">
                <a class="cleeng-facebook" href="#">
                    <img src="<?php echo base_path().drupal_get_path('module', 'cleeng'); ?>/images/facebook.png"  alt="Share on Facebook" title="Share on Facebook" />
                </a>
                <a class="cleeng-twitter" href="#">
                    <img src="<?php echo base_path().drupal_get_path('module', 'cleeng'); ?>/images/twitter.png"  alt="Share on Twitter" title="Share on Twitter" />
                </a>
                &nbsp;
                <span>Referral URL:</span>
                <span class="cleeng-referral-url"></span>
            </div>
        </div>
    </div>