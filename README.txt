// $Id: README.txt,v 1.0.0 2010/11/25 21:31:03 hass Exp $

Module: Cleeng
Author: GoalGorilla <www.goalgorilla.com/contact>

Description
===========
Adds the Cleeng functionality to your website.
Cleeng is a new way to monetize online content. Check out Cleeng.com.

Requirements
============
* Cleeng user account - cleeng.om
* Free upgrade to Cleeng publisher account - cleeng.com/my-account/upgrade
* Client ID and Client secret (Register new OAuth Client)- cleeng.com/developer

Installation
============
* Copy the 'cleeng' module directory in to your Drupal sites/all/modules directory as usual and enable it trough Modules (admin/build/modules).
* In the settings page (admin/settings/cleeng) enter your Cleeng Client ID en Client Secret codes (see Requirements). And save.
* Through Permissions (admin/user/permissions) set which Roles can administer and/or use the Cleeng functionality on nodes.

Usage
=======
* Create a new node where Cleeng functionality should be available
* Go to the ? Cleeng Content Widget? and make sure you login
* Select the content you want to hide with Cleeng
* Click on the ? Create Cleeng Content from selection?
* Choose your settings and click ?Save markers? 
* Markers will be added to your content
* Save your file

Your page will now have the Cleeng functionality available. You can confirm this by viewing the page in your browser.

Other (under construction)
==========
Terms and Conditions
http://staging.cleeng.com/p/terms-and-conditions

Privacy statement
http://staging.cleeng.com/p/privacy

