/*
Cleeng Plugin for Drupal
Back-End JS Library
Author: DG2ALL
*/

var CleengWidget = {
    // New Content Form pop-up
    newContentForm : {content: {}},
    editorBookmark : {},
    saveContentServiceURL : Drupal.settings.basePath+'?q=cleeng/ajax&backendWidget=true&cleengMode=saveContent',
	edCanvasId: 'edit-body',
	edCanvas: null,

    teserInputWatcher: function() {
        desc = jQuery('#cleeng-ContentForm-Description');
        if (desc.val().length > 110) {
            desc.val(desc.val().substring(0, 110));
        }
        jQuery('#cleeng-ContentForm-DescriptionCharsLeft').html(110 - desc.val().length);
    },
    toggleHasLayerDates: function() {
        if (jQuery('#cleeng-ContentForm-LayerDatesEnabled:checked').length) {
            disabled = false;
        } else {
            disabled = 'disabled';
        }
        jQuery('#cleeng-ContentForm-LayerStartDate, #cleeng-ContentForm-LayerEndDate')
            .attr('disabled', disabled)
            .datepicker("option", "disabled", disabled);
        jQuery('#cleeng-ContentForm-LayerStartTimeHour, #cleeng-ContentForm-LayerStartTimeMinute, #cleeng-ContentForm-LayerEndTimeHour, #cleeng-ContentForm-LayerEndTimeMinute')
            .attr('disabled', disabled);
    },
    toggleReferralProgramEnabled: function() {
        if (jQuery('#cleeng-ContentForm-ReferralProgramEnabled:checked').length) {
            jQuery('#cleeng-ContentForm-ReferralRateSlider').slider("enable");
        } else {
            jQuery('#cleeng-ContentForm-ReferralRateSlider').slider("disable");
        }
    },
    init : function(){
		CleengWidget.edCanvas = $('#'+CleengWidget.edCanvasId).get(0);
		
        jQuery(document).ajaxError(function(e, xhr, settings, exception) {
                //console.log(e);
                //console.log(xhr);
                //console.log(settings);
                //console.log(exception);
          if (settings.url == CleengWidget.saveContentServiceURL) {
            jQuery(this).text(e.toString());
          }
        });
        
        //CleengWidget.authUser();

        jQuery('#cleeng-login').click(function() {
            window.open(Drupal.settings.basePath + '?q=cleeng/ajax&backendWidget=true&cleengMode=auth&cleengPopup=1','CleengConfirmationPopUp', 'menubar=no,width=607,height=567,toolbar=no');
			return false;
        });
        jQuery('#cleeng-logout').click(function() {
            jQuery.post(
                Drupal.settings.basePath + '?q=cleeng/ajax&backendWidget=true&cleengMode=logout',
                function(resp) {
                   CleengWidget.authUser();
                }
            );
			return false;
        });

        //jQuery('#cleeng-ContentForm-Description').bind('change keyup keypress paste input', CleengWidget.teserInputWatcher);
        //jQuery(CleengWidget.edCanvas).bind('change keyup keypress paste input', CleengWidget.findContent);

        jQuery('#cleeng-ContentForm-LayerDatesEnabled').click(CleengWidget.toggleHasLayerDates);
        jQuery('#cleeng-ContentForm-ReferralProgramEnabled').click(CleengWidget.toggleReferralProgramEnabled);

        jQuery('#cleeng-edit-panel #edit-markcontent')
            .click(CleengWidget.createCleengContent)
            .hover(
                    function(){
                            jQuery(this).addClass('ui-state-hover');
                    },
                    function(){
                            jQuery(this).removeClass('ui-state-hover');
                    }
            ).mousedown(function(){
                            jQuery(this).parents('.fg-buttonset-single:first').find('.fg-button.ui-state-active').removeClass('ui-state-active');
                            if( jQuery(this).is('.ui-state-active.fg-button-toggleable, .fg-buttonset-multi .ui-state-active') ){jQuery(this).removeClass('ui-state-active');}
                            else {jQuery(this).addClass('ui-state-active');}
            })
            .mouseup(function(){
                    if(! jQuery(this).is('.fg-button-toggleable, .fg-buttonset-single .fg-button,  .fg-buttonset-multi .fg-button') ){
                            jQuery(this).removeClass('ui-state-active');
                    }
            });

        CleengWidget.newContentForm = jQuery('#cleeng-contentForm').dialog({
            autoOpen: false,
            height: 405,
            width: 400,
            modal: true,
            buttons: {
                'Save markers' : CleengWidget.newContentFormRegisterContent,
                'Cancel' : function() {
                    jQuery(this).dialog('close');
                }
            },
            close: function() {
            },
            hide: 'clip'
        });

        jQuery('#cleeng-ContentForm-PriceSlider').slider({
            animate:true,
            min:15,
            max:99,
            step:1,
            slide: function(event, ui) {
                jQuery('#cleeng-ContentForm-PriceValue').html(ui.value/100);
				jQuery('#cleeng-ContentForm-Price').val(ui.value/100);
            }
        });
        jQuery('#cleeng-ContentForm-ReferralRateSlider').slider({
            animate:true,
            min:5,
            max:30,
            step:1,
            slide: function(event, ui) {
                jQuery('#cleeng-ContentForm-ReferralRateValue').html(ui.value);
				jQuery('#cleeng-ContentForm-ReferralRate').val(ui.value/100);
            }
        });

        // autologin
        if (typeof CleengAutologin !== 'undefined') {
            if (CleengAutologin.available) {
                jQuery.getJSON(
                    Drupal.settings.basePath+'?q=cleeng/ajax&cleengMode=autologin&id=' + CleengAutologin.id
                        + '&key=' + CleengAutologin.key,
                    function(resp) {
                        if (resp.success) {
                            CleengWidget.authUser(false);
                        }
                    }
                );
            }else{
				$('#cleeng-login').parent().show();
				//CleengWidget.authUser();
			}
        }

        CleengWidget.findContent();
    },    
    authUser: function() {
        jQuery.getJSON(
            Drupal.settings.basePath+'?q=cleeng/ajax&backendWidget=true&cleengMode=getUserInfo',
            function(resp) {
                CleengWidget.userInfo = resp;
                jQuery('#cleeng-connecting').hide();
                if(resp == null || resp.auth == false) {
                        jQuery('#cleeng-logout').parent().hide();
                        jQuery('#cleeng-login').parent().show();
                        jQuery('#cleeng-auth-options').hide();
                        jQuery('#cleeng-notPublisher').hide();
                        jQuery('#edit-markcontent').attr('disabled', 'disabled')
                                                   .css('display', 'none');
						jQuery('#contenthistory-div').css('display', 'none');
                } else {
                        jQuery('#cleeng-login').parent().hide();
                        jQuery('#cleeng-logout').parent().show();
                        jQuery('#cleeng-username').html(resp.name);
                        if (resp.accountType != 'publisher') {
                            jQuery('#cleeng-notPublisher').show();
                            jQuery('#cleeng-auth-options').hide();
                        } else {
                            jQuery('#cleeng-notPublisher').hide();
                            jQuery('#cleeng-auth-options').show();
                        }
                        jQuery('#edit-markcontent').attr('disabled', false)
						                           .css('display', 'block');
						jQuery('#contenthistory-div').css('display', 'block');
                }
            }
        );
    },    
    showContentForm: function(content) {
        if ( typeof tinyMCE != 'undefined' && ( ed = tinyMCE.activeEditor ) && !ed.isHidden() ) {
            CleengWidget.editorBookmark = ed.selection.getBookmark();
        }
        content.contentId = typeof content.contentId === 'undefined' ? 0 : content.contentId;
        content.price = typeof content.price === 'undefined' ? '0.15' : content.price;
        content.shortDescription = typeof content.shortDescription === 'undefined' ? '' : content.shortDescription;
        content.referralProgramEnabled = typeof content.referralProgramEnabled === 'undefined' ? 0 : content.referralProgramEnabled;
        content.referralRate = typeof content.referralRate === 'undefined' ? 0.05 : content.referralRate;
        content.hasLayerDates = typeof content.hasLayerDates === 'undefined' ? 0 : content.hasLayerDates;
        content.layerStartDate = typeof content.layerStartDate === 'undefined' ? '' : content.layerStartDate;
        content.layerStartTimeHour = typeof content.layerStartTimeHour === 'undefined' ? '00' : content.layerStartTimeHour;
        content.layerStartTimeMinute = typeof content.layerStartTimeMinute === 'undefined' ? '00' : content.layerStartTimeMinute;
        content.layerEndDate = typeof content.layerEndDate === 'undefined' ? '' : content.layerEndDate;
        content.layerEndTimeHour = typeof content.layerEndTimeHour === 'undefined' ? '00' : content.layerEndTimeHour;
        content.layerEndTimeMinute = typeof content.layerEndTimeMinute === 'undefined' ? '00' : content.layerEndTimeMinute;
        CleengWidget.newContentForm.contentId = content.contentId;
        jQuery('#cleeng-ContentForm-Description').val(decodeURI(content.shortDescription));
        //jQuery('#cleeng-ContentForm-PriceSlider').slider('value', [content.price]);
        //jQuery('#cleeng-ContentForm-PriceSlider').slider({value: [content.price]});
		var pricePercentLeft = 358*(content.price-0.15)/(0.99-0.15);
		jQuery('#cleeng-ContentForm-PriceSlider .ui-slider-handle').css('left', pricePercentLeft+'px');
		jQuery('#cleeng-ContentForm-Price').val(content.price);
		
        jQuery('#cleeng-ContentForm-LayerStartDate').datepicker();
        jQuery('#cleeng-ContentForm-LayerEndDate').datepicker();
        jQuery('#cleeng-ContentForm-PriceValue').html(content.price);
        jQuery('#cleeng-ContentForm-ReferralRateValue').html(content.referralRate * 100);
        //jQuery('#cleeng-ContentForm-ReferralRateSlider').slider({value: [content.referralRate * 100]});
		jQuery('#cleeng-ContentForm-ReferralRate').val(content.referralRate);
		
        //jQuery('#cleeng-ContentForm-ReferralRateSlider').slider('value', content.referralRate * 100);
		var referralPercentLeft = 358*(content.referralRate-0.05)/(0.3-0.05);
		jQuery('#cleeng-ContentForm-ReferralRateSlider .ui-slider-handle').css('left', referralPercentLeft+'px');
        jQuery('#cleeng-ContentForm-ReferralProgramEnabled').attr('checked', content.referralProgramEnabled?'checked':null);

        jQuery('#cleeng-ContentForm-LayerStartDate').val(content.layerStartDate);
        jQuery('#cleeng-ContentForm-LayerEndDate').val(content.layerEndDate);
        jQuery('#cleeng-ContentForm-LayerDatesEnabled').attr('checked', content.hasLayerDates?'checked':null);
		jQuery('#cleeng-contentForm').css('display', 'block');
		
		$('#cleeng-ContentForm-LayerDates select').attr('disabled', false);
		$('#cleeng-ContentForm-LayerStartTimeHour').val(content.layerStartTimeHour);
		$('#cleeng-ContentForm-LayerStartTimeMinute').val(content.layerStartTimeMinute);
		$('#cleeng-ContentForm-LayerEndTimeHour').val(content.layerEndTimeHour);
		$('#cleeng-ContentForm-LayerEndTimeMinute').val(content.layerEndTimeMinute);
		
        CleengWidget.newContentForm.dialog('open');
    },
    isSelectionValid: function() {
        return !!(jQuery.trim(CleengWidget.getSelectedText()));
    },
    createCleengContent: function() {
        if (CleengWidget.isSelectionValid()) {
		  try{
            jQuery('#cleeng-contentForm input[type=text]').val('')
            jQuery('#cleeng_SelectionError').hide();
            var now = new Date();
            jQuery('#cleeng-ContentForm-LayerStartDate').val(
                jQuery.datepicker.formatDate('yy-mm-dd', now)
            );
            jQuery('#cleeng-ContentForm-LayerDates select.cleeng-ContentForm-timeselector').attr('disabled', 'disabled').val('00');
            
            now.setDate(now.getDate()+7);
            jQuery('#cleeng-ContentForm-LayerEndDate').val(
                jQuery.datepicker.formatDate('yy-mm-dd', now)
            );
            jQuery('#cleeng-ContentForm-LayerStartDate, #cleeng-ContentForm-LayerEndDate')
                .attr('disabled', 'disabled');
            jQuery('#cleeng-ContentForm-LayerDatesEnabled').attr('checked', false);
            jQuery('#cleeng-ContentForm-ReferralRateSlider').slider("disable");
            CleengWidget.showContentForm({});
		  }catch(e){alert('error')}
        } else {
            jQuery('#cleeng_SelectionError').show('pulsate');
        }
		
		return false;
    },
    newContentFormRegisterContent: function() {
		var _price = jQuery('#cleeng-ContentForm-Price').val();
		var _ReferralRate = jQuery('#cleeng-ContentForm-ReferralRate').val();
		var _url = '';//jQuery.trim(jQuery('#sample-permalink').html().split('<span')[0] + jQuery('#post_name').val());
        var content = {
            contentId: CleengWidget.newContentForm.contentId,
            pageTitle: jQuery('#title').val() + ' | ' + jQuery('#site-title').html(),
            price: _price,
            shortDescription: jQuery('#cleeng-ContentForm-Description').val(),
            url: _url
        };
        content.itemType = 'article';
        if (jQuery('#cleeng-ContentForm-LayerDatesEnabled:checked').length) {
            content.hasLayerDates = 1;
            content.layerStartDate = jQuery('#cleeng-ContentForm-LayerStartDate').val();
            content.layerStartTimeHour = jQuery('#cleeng-ContentForm-LayerStartTimeHour').val();
            content.layerStartTimeMinute = jQuery('#cleeng-ContentForm-LayerStartTimeMinute').val();
            content.layerEndDate = jQuery('#cleeng-ContentForm-LayerEndDate').val();
            content.layerEndTimeHour = jQuery('#cleeng-ContentForm-LayerEndTimeHour').val();
            content.layerEndTimeMinute = jQuery('#cleeng-ContentForm-LayerEndTimeMinute').val();
        }
        if (jQuery('#cleeng-ContentForm-ReferralProgramEnabled:checked').length) {
            content.referralProgramEnabled = 1;
            content.referralRate = _ReferralRate;
        }
        //CleengWidget.saveContent(content);
        CleengWidget.saveContentWithoutSubmit(content);
    },
	
	saveContentWithoutSubmit: function(content){
		if(content.id == null){
			content.id = '0';
		}
		
		re = new RegExp('\\[cleeng_content([^\\[\\]]+?)id="' + content.contentId + '"([\\S\\s]*?)\\]([\\S\\s]*?)\\[\\/cleeng_content\\]', 'mi');
		var text = CleengWidget.getEditorText();
		
		if (text.match(re)) {
			CleengWidget.updateStartMarker(content);
		} else {
			CleengWidget.addMarkers(content);
		}
		CleengWidget.newContentForm.dialog('close');
	},
	
    saveContent: function(content) {        
        jQuery.post(
            CleengWidget.saveContentServiceURL,
            content,
            function(respObj) {
                //var respObj = JSON.parse(resp);
                CleengWidget.lastRespObj = respObj;
                if (respObj[content.contentId].contentSaved == true) {
                    content.contentId = respObj[content.contentId].contentId;
                    re = new RegExp('\\[cleeng_content([^\\[\\]]+?)id="' + content.contentId + '"([\\S\\s]*?)\\]([\\S\\s]*?)\\[\\/cleeng_content\\]', 'mi');
                    if (CleengWidget.getEditorText().match(re)) {
                        CleengWidget.updateStartMarker(content);
                    } else {
                        CleengWidget.addMarkers(content);
                    }
                    CleengWidget.newContentForm.dialog('close');
                }
            },
            "json"
        );
    },
    addMarkers: function(content) {
        CleengWidget.replaceSelectedText(CleengWidget.getStartMarker(content) + CleengWidget.getSelectedText() + '[/cleeng_content]');
        CleengWidget.findContent();
    },
    getStartMarker: function(content) {
		if(content.contentId == '0'){
			content.contentId = '-'+Math.floor(new Date().getTime()/1000);
		}
	
        var startMarker =  '[cleeng_content id=\"'+ content.contentId +'\"';
        startMarker += ' price=\"' + encodeURI(content.price) + '\"';
        startMarker += ' description=\"' + content.shortDescription + '\"';

        if (content.referralProgramEnabled && content.referralRate) {
            startMarker += ' referral=\"' + content.referralRate + '\"';
        }

        if (content.hasLayerDates && content.layerStartDate && content.layerEndDate) {
            startMarker += ' ls=\"' + content.layerStartDate + ' ' + content.layerStartTimeHour +':'+content.layerStartTimeMinute + '\" le=\"' + 
                                      content.layerEndDate + ' ' + content.layerEndTimeHour +':'+content.layerEndTimeMinute + '\"';
        }
		
        startMarker += ']';
        return startMarker;
    },
    updateStartMarker: function(content) {
        marker = CleengWidget.getStartMarker(content);
        re = new RegExp('\\[cleeng_content([^\\[\\]]+?)id=\\"' + content.contentId + '\\"(.)*?\\]', 'mi');
        editorText = CleengWidget.getEditorText();
		
        editorText = editorText.replace(re, marker);
        CleengWidget.setEditorText(editorText);
        CleengWidget.findContent(editorText);
    },
    findContent: function(editorText) {
		if(editorText == null){
			editorText = CleengWidget.getEditorText();
		}
        
		var matches = editorText.match(/\[cleeng_content([\S\s])+?\]/igm);
        contentList = '';
		if(matches){
			contentElements = jQuery(matches);
			contentElements.each(function(i,v) {
				var idattr = v.match(/id=\"-?(\d+?)\"/i);
				
				if (idattr && idattr[1]) {
					id = idattr[1];
					if(idattr[0].indexOf('-')>0){
						id = -1*parseInt(id);
					}
					var price = v.match(/price=\"(\d+[\.]{1}\d+?)\"/i);
					if (price && price[1]) {
						price = price[1];
					} else {
						price = 0;
					}
					contentList += '<li>id: ' + (id<0 ? 'N/A' : id) + ' (price: ' + price +
							') <a href="javascript:CleengWidget.editContent(' + id + ')">edit</a> ' +
							'<a href="javascript:CleengWidget.removeMarkers(' + id + ')">remove</a></li>';
				}
			});
		}
		
        if (contentList != '') {
            jQuery('#cleeng_NoContent').hide();
            jQuery('#cleeng-ContentList').show().find('ol').html(contentList);
        } else {
            jQuery('#cleeng_NoContent').show();
            jQuery('#cleeng-ContentList').hide();
        }
    },

    getContentFromEditor: function(contentId) {
        editorText = CleengWidget.getEditorText();
        re = new RegExp('\\[cleeng_content([^\\[\\]]+?)id=\\"' + contentId + '\\"([\\S\\s]*?)\\]([\\S\\s]*?)\\[\\/cleeng_content\\]', 'mi');
        ct = re.exec(editorText);

        if (ct && ct[0]) {
            var opening = ct[0].match(/\[cleeng_content\s+(.*)?=(.*)?\]/gi);
            window.test = opening[0];
            var id = opening[0].match(/id=\"(.*?)\"/i);
            if (id && id[1]) {
                var content = {
                    contentId: id[1]
                };
                var price = opening[0].match(/price=\"(.*?)\"/i);
                var description = opening[0].match(/description=\"(.*?)\"/i);
                var referral = opening[0].match(/referral=\"(.*?)\"/i);
                var ls = opening[0].match(/ls=\"(.*?)\"/i);
                var le = opening[0].match(/le=\"(.*?)\"/i);

                if (price && price[1]) {
                    content.price = price[1];
                } else {
                    content.price = 0;
                }
                if (description && description[1]) {
                    content.shortDescription = description[1];
                } else {
                    content.shortDescription = '';
                }
                if (referral && referral[1]) {
                    content.referralProgramEnabled = 1;
                    content.referralRate = referral[1];
                } else {
                    content.referralProgramEnabled = 0;
                }
                if (ls && ls[1] && le && le[1]) {
                    content.hasLayerDates = 1;
                    content.layerStartDate = ls[1];
					var pos = content.layerStartDate.indexOf(' ');
					if(pos > 0){
						var time = content.layerStartDate.substring(pos+1);
						var pos1 = time.indexOf(':');
						content.layerStartTimeHour = time.substring(0, pos1);
						content.layerStartTimeMinute = time.substring(pos1+1);
						content.layerStartDate = content.layerStartDate.substring(0, pos);
					}
					
					content.layerEndDate = le[1];
					pos = content.layerEndDate.indexOf(' ');
					if(pos > 0){
						var time = content.layerEndDate.substring(pos+1);
						var pos1 = time.indexOf(':');
						content.layerEndTimeHour = time.substring(0, pos1);
						content.layerEndTimeMinute = time.substring(pos1+1);
						content.layerEndDate = content.layerEndDate.substring(0, pos);
					}
                }

                return content;
            }
        }
        return null;
    },
	
    editContent: function(id) {
        var content = CleengWidget.getContentFromEditor(id);
        if (!content) {
            return;
        }
        CleengWidget.showContentForm(content);
        CleengWidget.teserInputWatcher();
    },
    removeMarkers: function(contentId) {
        var re = new RegExp('\\[cleeng_content([^\\[\\]]+?)id="' + contentId + '"([\\S\\s]*?)\\]([\\S\\s]*?)\\[\\/cleeng_content\\]', 'i');

        editorText = CleengWidget.getEditorText();
        rpl = re.exec(editorText);
        if (rpl && rpl[0] && rpl[3]) {
            editorText = editorText.replace(rpl[0], rpl[3])
        }
        CleengWidget.setEditorText(editorText);
        CleengWidget.findContent(editorText);
    },

    // helper functions for accessing editor
    // works for TinyMCE and textarea ("HTML Mode")
    getEditorText: function() {
		var html = null;
        if ( typeof tinyMCE != 'undefined' && ( ed = tinyMCE.activeEditor ) && !ed.isHidden() ) {
            html = ed.getContent();
        } else if ( typeof CKEDITOR != 'undefined' && ( ed = CKEDITOR.instances[CleengWidget.edCanvasId] ) ) {
			html = ed.getData();
        } else if ( typeof FCKeditorAPI != 'undefined' && ( ed = FCKeditorAPI.GetInstance(CleengWidget.edCanvasId) ) ) {
			html = ed.GetData();
        } else {
            return jQuery( CleengWidget.edCanvas ).val();
        }
		
		return html.replaceAll('&quot;', '"');
    },
    setEditorText: function(text) {
        if ( typeof tinyMCE != 'undefined' && ( ed = tinyMCE.activeEditor ) && !ed.isHidden() ) {
			text = text.replaceAll('"', '&quot;');
            ed.setContent(text);
        } else if ( typeof CKEDITOR != 'undefined' && ( ed = CKEDITOR.instances[CleengWidget.edCanvasId] ) ) {
			text = text.replaceAll('"', '&quot;');
			ed.setData(text);
        } else if ( typeof FCKeditorAPI != 'undefined' && ( ed = FCKeditorAPI.GetInstance(CleengWidget.edCanvasId) ) ) {
			text = text.replaceAll('"', '&quot;');
			ed.SetData(text);
        } else {
            jQuery( CleengWidget.edCanvas ).val(text);
        }
    },
	
    getSelectedText: function() {
        if ( typeof tinyMCE != 'undefined' && ( ed = tinyMCE.activeEditor ) && !ed.isHidden() ) {
            ed.focus();
            ed.selection.moveToBookmark(CleengWidget.editorBookmark);
            return jQuery.trim(ed.selection.getContent());
        } else if ( typeof CKEDITOR != 'undefined' && ( ed = CKEDITOR.instances[CleengWidget.edCanvasId] ) ) {
			var text = CleengWidget.getCKEditorSelectionText(ed);
			return text;
        } else if ( typeof FCKeditorAPI != 'undefined' && ( ed = FCKeditorAPI.GetInstance(CleengWidget.edCanvasId) ) ) {
			var selection = (ed.EditorWindow.getSelection ? ed.EditorWindow.getSelection() : ed.EditorDocument.selection);
		   
			if(selection.createRange) {
				var range = selection.createRange();
				var html = range.htmlText;
			}
			else {
				var range = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
				var clonedSelection = range.cloneContents();
				var div = document.createElement('div');
				div.appendChild(clonedSelection);
				var html = div.innerHTML;
			}
		   
			return html;
        } else {
            selStart = CleengWidget.edCanvas.selectionStart;
            selEnd = CleengWidget.edCanvas.selectionEnd;
            selLen = selEnd - selStart;
            return jQuery.trim(jQuery(CleengWidget.edCanvas).val().substr(selStart, selLen));
        }
		
		return '';
    },
	
	getCKEditorSelectionText: function(ed){
		var selection = ed.getSelection();
		if( selection ){
		  var bookmarks = selection.createBookmarks(),
			 range = selection.getRanges()[ 0 ],
			 fragment = range.clone().cloneContents();

		  selection.selectBookmarks( bookmarks );

		  var retval = "",
			 childList = fragment.getChildren(),
			 childCount = childList.count();
		  for ( var i = 0; i < childCount; i++ )
		  {
			 var child = childList.getItem( i );
			 retval += ( child.getOuterHtml?
				child.getOuterHtml() : child.getText() );
		  }
		  return retval;
		}
		return '';
	},
	
	setContentForCKEditor: function(ed, content){
		var selection = ed.getSelection();
		var range = selection.getRanges()[ 0 ];
		range.deleteContents();
		var node = new CKEDITOR.dom.node($('<span>'+content+'</span>').get(0));
		range.insertNode(node);
	},
	
    replaceSelectedText: function(replacement) {
        if ( typeof tinyMCE != 'undefined' && ( ed = tinyMCE.activeEditor ) && !ed.isHidden() ) {
            ed.focus();
            ed.selection.moveToBookmark(CleengWidget.editorBookmark);
            ed.selection.setContent(replacement);
        } else if ( typeof CKEDITOR != 'undefined' && ( ed = CKEDITOR.instances[CleengWidget.edCanvasId] ) ) {
			CleengWidget.setContentForCKEditor(ed, replacement);
        } else if ( typeof FCKeditorAPI != 'undefined' && ( ed = FCKeditorAPI.GetInstance(CleengWidget.edCanvasId) ) ) {
			var selection = (ed.EditorWindow.getSelection ? ed.EditorWindow.getSelection() : ed.EditorDocument.selection);
			var range = null;
			if(selection.createRange) {
				var range = selection.createRange();
			}
			else {
				var range = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
			}
			range.deleteContents();
			node = ed.CreateElement($('<span>'+replacement+'</span>').get(0));
			//range.insertNode(node);
			
		} else {
            selStart = CleengWidget.edCanvas.selectionStart;
            selEnd = CleengWidget.edCanvas.selectionEnd;
            editorText = CleengWidget.getEditorText();
            newContent = editorText.substr(0, selStart)
                         + replacement
                         + editorText.substr(selEnd);
            jQuery(CleengWidget.edCanvas).val(newContent);
        }
    }
};

String.prototype.replaceAll  = function(s1,s2){   
	return this.replace(new RegExp(s1,"gm"),s2);
}

jQuery(CleengWidget.init);