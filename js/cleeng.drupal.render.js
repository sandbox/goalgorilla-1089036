/*
Cleeng Plugin for Wordpress
Front-End JS Library
Author: DG2ALL
*/
var CleengFEDrupalWidget = {

    contentIds: [],
    popupWindow: null,

	lastPurchaseContentId: null,
	
    init: function() {
        jQuery(document).ajaxError(function(e, xhr, settings, exception) {
            if (typeof(console) !== 'undefined') {
                console.log(e);
                console.log(xhr);
                console.log(settings);
                console.log(exception);
            }
        });

        CleengFEDrupalWidget.authUser(true, function(){
			CleengFEDrupalWidget.getContentInfo();
		});

        jQuery('.cleeng-login').click(function() {CleengFEDrupalWidget.logIn();return false;});
        jQuery('.cleeng-logout').click(function() {CleengFEDrupalWidget.logOut();return false;});
        
        CleengFEDrupalWidget.contentIds = [];

        jQuery('.cleeng-layer').each(function() {
            var contentId = jQuery(this).attr('id').split('-')[2];
            CleengFEDrupalWidget.contentIds.push(contentId);
            jQuery('#cleeng-layer-' + contentId + ' .cleeng-buy')
                .click(function() {
                    CleengFEDrupalWidget.purchaseContent(contentId);
                    return false;
                });
            jQuery('#cleeng-nolayer-' + contentId + ' .cleeng-vote-liked').click(function() {
                jQuery.post(
                    Drupal.settings.basePath + '?q=cleeng/ajax&cleengMode=vote&liked=1&contentId=' + contentId,
                    function() {
                        CleengFEDrupalWidget.getContentInfo();
                    }
                );
                return false;
            });
            jQuery('#cleeng-nolayer-' + contentId + ' .cleeng-vote-didnt-like').click(function() {
                jQuery.post(
                    Drupal.settings.basePath + '?q=cleeng/ajax&cleengMode=vote&liked=0&contentId=' + contentId,
                    function() {
                        CleengFEDrupalWidget.getContentInfo();
                    }
                );
                return false;
            });
        });

        jQuery('.cleeng-rating a').click(function() {
            return false;
        });

        jQuery('.cleeng-share').hide();

        jQuery('.cleeng-share a').click(function() {
            window.open(jQuery(this).attr('href'), 'shareWindow', 'menubar=no,width=607,height=567,toolbar=no');
            return false;
        });

        
        jQuery('.cleeng-auth, .cleeng-noauth').hide();

        jQuery('.cleeng-it').click(function() {
            parent = jQuery(this).parents('.cleeng-nolayer');
            parent.find('.cleeng-share').show();
            contentId = parent.attr('id').split('-')[2];
            if (parseInt(contentId)) {
                jQuery.post(
                    Drupal.settings.basePath + '?q=cleeng/ajax&cleengMode=refer&contentId=' + contentId,
                    function(res) {                        
                        CleengFEDrupalWidget.getContentInfo();
                    }
                );
            }
            jQuery(this).hide();
            return false;
        });

        // autologin
        if (typeof CleengAutologin !== 'undefined') {
            if (CleengAutologin.available) {
                jQuery.getJSON(
                    Drupal.settings.basePath+'?q=cleeng/ajax&cleengMode=autologin&id=' + CleengAutologin.id
                        + '&key=' + CleengAutologin.key,
                    function(resp) {
                        if (resp && resp.success) {
                            CleengFEDrupalWidget.authUser(false);
                        }
                    }
                );
            }
        }
    },
    authUser: function(init, callback) {
        jQuery.getJSON(
            Drupal.settings.basePath+'?q=cleeng/ajax&cleengMode=getUserInfo',
            function(resp) {
				if(resp != null && resp.auth == false){
					resp = null;
				}
			
                CleengFEDrupalWidget.userInfo = resp;
                jQuery('.cleeng-nolayer').hide();
                jQuery('.cleeng-layer').show();
                if(resp == null) {
                    jQuery('.cleeng-auth').hide();
                    jQuery('.cleeng-noauth').show();
                } else {
                    if (!init) {
                        CleengFEDrupalWidget.getContentInfo();
                    }
                    jQuery('.cleeng-username').html(resp.name);                    
                    jQuery('.cleeng-auth').show();
                    jQuery('.cleeng-noauth').hide();
                    if (parseInt(resp.freeContentViews)) {
                        jQuery('.cleeng-free-content-views span').text(resp.freeContentViews);
                    } else {
                        jQuery('.cleeng-free-content-views span').hide();
                    }
					
					if(callback){
						callback.apply();
					}
                }
            }
        );
    },
    logIn: function() {
        if (this.popupWindow) {
            this.popupWindow.close();
        }
        this.popupWindow = window.open(Drupal.settings.basePath + '?q=cleeng/ajax&cleengMode=auth&cleengPopup=1','CleengConfirmationPopUp', 'menubar=no,width=607,height=567,toolbar=no');
    },
    logOut: function() {
        jQuery.post(
            Drupal.settings.basePath + '?q=cleeng/ajax&cleengMode=logout',
            function(resp) {
                CleengFEDrupalWidget.authUser();
            }
        );
    },
    getContentInfo: function() {
            var content = {};            
            jQuery('.cleeng-layer').each(function(index, ele) {
                var id = jQuery(this).attr('id');
                if (id && typeof id.split('-')[2] !== 'undefined') {
                    id = id.split('-')[2];
					content['content['+index+'][id]'] = id;
					content['content['+index+'][postId]'] = jQuery(this).data('postId');
					content['content['+index+'][page]'] = jQuery(this).data('page');
					/*
                    content.push({
                        id: id,
                        postId: jQuery(this).data('postId'),
                        page: jQuery(this).data('page')
                    });*/
                }
            });
            jQuery.post(
                Drupal.settings.basePath + '?q=cleeng/ajax&cleengMode=getContentInfo',
                content,
                function(resp) {                                        
                    CleengFEDrupalWidget.contentInfo = resp;
                    CleengFEDrupalWidget.showPurchasedContent();
                },
            "json"
        );
    },
    purchaseContent: function(contentId) {
		CleengFEDrupalWidget.lastPurchaseContentId = contentId;
        if (this.popupWindow) {
            this.popupWindow.close();
        }
        this.popupWindow = window.open(Drupal.settings.basePath + '?q=cleeng/ajax&cleengMode=purchase&contentId=' + contentId + '&cleengPopup=1','CleengConfirmationPopUp', 'menubar=no,width=607,height=567,toolbar=no');
    },
	
    markContentPurchased: function(contentId) { 
		contentId = (contentId == null) ? CleengFEDrupalWidget.lastPurchaseContentId : contentId;
		if(contentId && CleengFEDrupalWidget.contentInfo && CleengFEDrupalWidget.contentInfo[contentId]){
			CleengFEDrupalWidget.contentInfo[contentId].purchased = true;
		}
	},
	
    showPurchasedContent: function() {        
        if (CleengFEDrupalWidget.contentInfo) {            
            jQuery.each(CleengFEDrupalWidget.contentInfo, function(k, v){
                var layerId = '#cleeng-layer-' + k;
                var noLayerId = '#cleeng-nolayer-' + k;
                jQuery(layerId + ' .cleeng-price').html(v.currencySymbol + '' + v.price.toFixed(2));
                jQuery('.cleeng-stars', layerId).attr('class', 'cleeng-stars').addClass('cleeng-stars-' + Math.round(v.averageRating));
                jQuery('.cleeng-stars', noLayerId).attr('class', 'cleeng-stars').addClass('cleeng-stars-' + Math.round(v.averageRating));                
                if (v.purchased == true && v.content) {
                    if (v.referralProgramEnabled) {
                        jQuery('.cleeng-referral-url', noLayerId).text(v.referralUrl).parent().hide();
                        jQuery('a.cleeng-facebook').attr('href',
                            'http://www.facebook.com/sharer.php?u='
                                + encodeURI(v.referralUrl) + '&t='
                                + encodeURI(v.pageTitle)
                        );
                        jQuery('a.cleeng-twitter').attr('href',
                            'http://twitter.com/?status='
                                + encodeURI(v.shortUrl)
                        );
                    } else {                        
                        jQuery('.cleeng-referral-url', noLayerId).text(v.shortUrl);
                        jQuery('a.cleeng-facebook').attr('href',
                            'http://www.facebook.com/sharer.php?u='
                                + encodeURI(v.shortUrl) + '&t='
                                + encodeURI(v.pageTitle)

                        );
                        jQuery('a.cleeng-twitter').attr('href',
                            'http://twitter.com/?status='
                                + encodeURI(v.shortUrl)
                        );
                    }                    
                    if (v.canVote) {
                        jQuery('.cleeng-vote-liked', noLayerId).show();
                        jQuery('.cleeng-vote-didnt-like', noLayerId).show();
                    } else {
                        jQuery('.cleeng-vote-liked', noLayerId).hide();
                        jQuery('.cleeng-vote-didnt-like', noLayerId).hide();
                    }

                    if (v.referralProgramEnabled) {
                        jQuery('.cleeng-referral-rate', noLayerId).show()
                            .find('span').text(Math.round(v.referralRate*100)+'%');
                    } else {
                        jQuery('.cleeng-referral-rate', noLayerId).hide();
                    }

                    jQuery(layerId).hide();
                    jQuery('.cleeng-content', noLayerId).html(v.content);
                    jQuery(noLayerId).show();
                    
                    jQuery(noLayerId + ' .cleeng-noauth').hide();
                    jQuery(noLayerId + ' .cleeng-auth').show();
                }
            });
        }
    }
}

jQuery(CleengFEDrupalWidget.init);

var CleengWidget = CleengFEDrupalWidget;